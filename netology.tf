# Prosvetov Ivan

provider "aws" {
  region = "us-west-2"
  version = "~> 2.18"
}

resource "aws_cloudwatching_log_group" "lambda" {
  name = "/aws/lambda/netology"

retention_in_days = 1
}

