<<<<<<< HEAD
1.1 Ivan Prosvetov введение в DevOps
=======
# **netologyProject**
1.1. Введение в DevOps - Prosvetov Ivan
______________
[netology.tf](https://github.com/Darkpunks/netologyProject/blob/main/terraform.png) 

[netology.bh](https://github.com/Darkpunks/netologyProject/blob/main/bash.png)

[netology.md](https://github.com/Darkpunks/netologyProject/blob/main/markdown.png)

[netology.yaml](https://github.com/Darkpunks/netologyProject/blob/main/yaml.png)

[netology.jsonnet](https://github.com/Darkpunks/netologyProject/blob/main/jsonnet.png)

______________________________________________________________________________
2. Описание жизненного цикла задачи

2.1.Определение требований 

Описание задачи, её постановка/требования/особенности.


2.2.Спецификации 

Создание Технического задания для проекта.

2.3.Проектирование 

Создание первых прототипов (Бета-версия), использование разных вариантов и способов.

2.4.Реализация 

Выпуски стабильный версий, раскатка на рабочем железе, обкатывание системы. 

2.5.Тестирование 

Отслеживание багов, повышение стабильности работы. 

2.6.Сопровождение 

Обновление программы, повышение стабильности программы. 

2.7.Развитие 

Добавление новых функций и улучшение работоспособности, по требованию заказчика

____________________________________________________________________

3.1 Системы контроля версий

 3.1.1 В файле .gitignore будут проигнорированы следующие файлы:

 3.1.1.2. Во всех вложенных Директориях - */.terraform/ :

3.1.1.3. .tfstate - В этот файл записывается вся инфраструктура, которую мы создали;


3.1.1.4. crash.log - это файл журнала с журналами отладки из сеанса, в который при сбое сохраняет терраформ;


3.1.1.5. .tfvars -  конфиг с значением переменных, часто является секретным, так как содержит данные пользователей (пароли\логины);


3.1.1.6. override.tf, override.tf.json - позволяет переобределить метод базового класса ;


3.1.1.7. .terraformrc, terraform.rc - конфигурационные файлы интерфейса командной строки, которые мы игнорируем;

4.1 Github https://github.com/Darkpunks/netologyProject
4.2 Gitlab https://bitbucket.org/darkpunks94/netologyproject/src/master/
4.3 Bitbucket https://bitbucket.org/darkpunks94/netologyproject/src/master/




